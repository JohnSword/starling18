/**
 *	Copyright (c) 2014 Devon O. Wolfgang
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	THE SOFTWARE.
 */

package starling.filters {

	import com.element.oimo.ogsl.OGSL;

	import flash.display3D.Context3D;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;

	import starling.textures.Texture;

	/**
	 */
	public class OGSLFilter extends BaseFilter {

//		private var fc0 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] );

		private var ogsl : OGSL;
		private var count : Number = 0;


		/**
		 */
		public function OGSLFilter() {
			super( 1 );
		}


		/** Set AGAL */
		override protected function setAgal() : void {

			ogsl = new OGSL();
			ogsl.setLogFunction( function( s : String ) : void { // set log function
				trace( s );
			} );


			var vertexShader : String = <![CDATA[
varying tpos:vec2;
varying pos:vec2;
varying uv:vec2;
program vertex {
	attribute position:vec3;
	attribute texCoord:vec2;
	uniform matrix:mat4x4;

	function main():void {
		// 4x4 matrix transform to output space
		var pos:vec4 = mul(vec4(position, 1), matrix);
		output = pos;
		this.tpos = pos.xy;
		// screen position
		this.pos = position.xy;
		// pass texture coordinates to fragment program
		this.uv = texCoord;
	}
}
]]>;


			var displayFragmentShader : String = <![CDATA[
program fragment {
	
	uniform tex1 : texture;
	uniform position : vec2;
	uniform time:float;

    function main():void {
		var  color : vec4 = tex2D ( tex1, uv, clamp, linear, nomip );

		var PI:float = 3.14159265359;

		var iResolution:vec2 = vec2(640,960);
		var p:vec2 = vec2(pos.x / iResolution.x, pos.y / iResolution.y);

		var col1:vec3 = vec3(0,0,0);
		var col2:vec3 = vec3(0,0,1);
		var col3:vec3 = vec3(0,1,0);
		var bgCol:vec3 = vec3(1,1,1);

//		var ret:vec3 = bgCol;
//		ret = mix(ret, col1, rectangle(r, vec2(0, 0), vec2(0.1, 0.1)) );

//		var pixel:vec3;
//		if(p.x < 0.5) {
//			pixel = col1;
//		} else {
//			pixel = col2;
//		}

		if(color.a == 1) {
			color.rgb = vec3(1,1,1);
		}

		output = color;
    }

	function randomNoise(p:vec2):float {
	  return fract(6791*sin(47*p.x+p.y*9973));
	}

	function interpolatedNoise(p:vec2) : float {
		var s:vec2 = smoothstep(0, 1, fract(p));
		var q11:float = smoothNoise(vec2(floor(p.x), floor(p.y)));
		var q12:float = smoothNoise(vec2(floor(p.x), ceil(p.y)));
		var q21:float = smoothNoise(vec2(ceil(p.x), floor(p.y)));
		var q22:float = smoothNoise(vec2(ceil(p.x), ceil(p.y)));
		var r1:float = mix(q11, q21, s.x);
		var r2:float = mix(q12, q22, s.x);
		return mix (r1, r2, s.y);
	}

	function smoothNoise(p:vec2) : float {
		var nn:vec2 = vec2(p.x, p.y+1);
		var ee:vec2 = vec2(p.x+1, p.y);
		var ss:vec2 = vec2(p.x, p.y-1);
		var ww:vec2 = vec2(p.x-1, p.y);
		var cc:vec2 = vec2(p.x, p.y);
		var sum:float = 0;
		sum += randomNoise(nn)/8;
		sum += randomNoise(ee)/8;
		sum += randomNoise(ss)/8;
		sum += randomNoise(ww)/8;
		sum += randomNoise(cc)/2;
		return sum;
	}

	function noise(p:vec2) : float {
	    return tex2D(tex1,p,clamp).r;
	}

	function circle(color:vec3,screen:vec2,center:vec2,radius:float) : vec3 {
		var position:vec2 = pos.xy - center;
		if (length(position) > radius) {
			discard;
		}
		return color;
	}

	function disk(r:vec2, center:vec2, radius:float) : float {
		var alias:float = 0.005;
		return 1.0 - smoothstep(radius-alias, radius+alias, length(r-center));
	}

	function renderFluid(color:vec3, dVofBlurred:vec2, &outDuv:vec2, &outDiffuse:vec3, &outSpecular:float):void {
		var normal:vec3 = normalize(vec3(-dVofBlurred, 0.1));
		var lightDir:vec3 = normalize(vec3(1.0, 3.0, -1.0));
		var lightDirRev:vec3 = normalize(vec3(-0.3, -0.7, -2.0));
		var lightDirRev2:vec3 = normalize(vec3(1.2, -0.8, -1.0));
		var dotLight:vec3;
		dotLight.x = -dot(lightDir, vec3(0, 0, -1) + 2 * normal.z * normal);
		dotLight.y = -dot(lightDirRev, normal);
		dotLight.z = -dot(lightDirRev2, normal);
		dotLight = max(0, dotLight);
		var brightness:float = min(1, 0.4 + dotLight.y * 0.6 + max(0, dotLight.z - 0.5) * 1.5);
		var specular:float = pow(max(0, dotLight.x - 0.5) * 2, 2) + pow(dot(normal.xy, normal.xy), 12) * 0.5;
		outDuv = -normal.xy * 2.0;
		outDiffuse = color * brightness;
		outSpecular = specular;
	}

}
]]>;

			ogsl.compile( vertexShader + " " + displayFragmentShader );

			VERTEX_SHADER = ogsl.getVertexAGAL();
			FRAGMENT_SHADER = ogsl.getFragmentAGAL();

		}


		/** Activate */
		protected override function activate( pass : int, context : Context3D, texture : Texture ) : void {

			ogsl.setContext3D( context );
//			ogsl.setTexture( "tex1", texture.base );
			ogsl.setDefaultConstants();
//			ogsl.setVertexConstantsFromMatrix( "matrix", new Matrix3D() );
//			ogsl.setFragmentConstantsFromVector( "uv", Vector.<Number>( [ 1, 0 ] ) );
			count++;
			ogsl.setFragmentConstantsFromNumber( "time", count );

//			const fc1 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] )

//			var vtxB : VertexBuffer3D = context.createVertexBuffer( 4, 5 );
//			vtxB.uploadFromVector( Vector.<Number>( [
//													-1, 1, 0, 0, 0,
//													1, 1, 0, 1, 0,
//													-1, -1, 0, 0, 1,
//													1, -1, 0, 1, 1,
//													] ), 0, 4 );

			// Create a vertex buffer of vertex positions for one
			// triangle with 3 attributes (X, Y, and Z) per vertex
//			var positions : VertexBuffer3D = context.createVertexBuffer( 3, 3 );
//
//			// Upload the triangle's positions to the vertices from 0 through 3
//			positions.uploadFromVector( new <Number>[
//										0, 1, 0, // top-center
//										-1, -1, 0, // bottom-left
//										1, -1, 0 // bottom-right
//										], 0, 3 );
//
//			ogsl.setVertexBuffer( "position", positions, 0, Context3DVertexBufferFormat.FLOAT_3 );
//			ogsl.setVertexBuffer( "position", vtxB, 0, Context3DVertexBufferFormat.FLOAT_3 );
//			ogsl.setVertexBuffer( "texCoord", vtxB, 3, Context3DVertexBufferFormat.FLOAT_2 );

//			var vertexBuffer : VertexBuffer3D = context.createVertexBuffer( 4, 3 );
//			vertexBuffer.uploadFromVector( Vector.<Number>( [
//															-1, -1, 0, // x, y, z
//															1, -1, 0,
//															-1, 1, 0,
//															1, 1, 0,
//															] ), 0, 4 );
//			ogsl.setVertexBuffer( "position", vertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3 );

			super.activate( pass, context, texture );
		}

	}
}
