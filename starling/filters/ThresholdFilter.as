﻿/**
 *	Copyright (c) 2012 Andy Saia
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	THE SOFTWARE.
 */

package starling.filters {
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;
	import starling.textures.Texture;

	public class ThresholdFilter extends FragmentFilter {
		private var shaderProgram : Program3D;
		private var _threshold : Number = .8;
		private var _multiplier : Number = 30;
		private var thresholdValues : Vector.<Number>;


		public function ThresholdFilter( threshold : Number, multiplier : Number ) {
			_threshold = threshold;
			_multiplier = multiplier;
			thresholdValues = Vector.<Number>( [ 0, 0, _multiplier, _threshold ] );
		}


		public override function dispose() : void {
			if ( shaderProgram )
				shaderProgram.dispose();
			super.dispose();
		}


		protected override function createPrograms() : void {
			var vertexShaderString : String =
			"m44 op, va0, vc0 \n" + // 4x4 matrix transform to output space
			"mov v0, va1     \n"; // pass texture coordinates to fragment program

			var fragmentShaderString : String =
//				"tex ft1, v0, fs0 <2d, linear, nomip> \n" + // just forward texture color
//				"sub ft1 ft1 fc1.w\n" + // subtracts threshold value from the texture data's alpha channel
//				"kil ft1.w \n" + // haltes execution for alpah values less then zero
//				"add ft1 ft1 fc1.w\n" + // adds back threshold value to texture data's alpha channel
//				"mov oc, ft1"; //outputs the resulting image
			"tex ft1, v0, fs0 <2d, linear, nomip> \n" +
			"sub ft1.w ft1.w fc1.w\n" + // subtract threshold
			"kil ft1.w \n" + // delete pixels less than zero
			"mul ft1.w ft1.w fc1.z \n" + //multiply to ramp back up to 100%
			"mov oc, ft1"; //outputs the resulting image

			//var fragmentShaderString:String =
			//	"tex ft1, v0, fs0 <2d, linear, nomip> \n" + 
			//	"sub ft1 ft1 fc1.w\n" + // subtract threshold
			//	"kil ft1.w \n" + // delete pixels less than zero
			//	"mul ft1.a ft1.a fc1.z \n"+//multiply to ramp back up to 100% - but it's altering color instead of just alpha WHY???
			//	"mov oc, ft1"; //outputs the resulting image


			shaderProgram = assembleAgal( fragmentShaderString, vertexShaderString );
		}


		protected override function activate( pass : int, context : Context3D, texture : Texture ) : void {
			// already set by super class:
			//
			// vertex constants 0-3: mvpMatrix (3D)
			// vertex attribute 0:   vertex position (FLOAT_2)
			// vertex attribute 1:   texture coordinates (FLOAT_2)
			// texture 0:            input texture


			//context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, thresholdValues, 1); //sets this vector to fc0

			//var valueToSet:Vector.<Number> = Vector.<Number>([0,0,0, _threshold]);
			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 1, thresholdValues, 1 ); //sets this vector to fc1
			context.setProgram( shaderProgram );
		}


		override protected function deactivate( pass : int, context : Context3D, texture : Texture ) : void {
		}


		//---------------
		//  getters and setters
		//---------------

		public function get threshold() : Number {
			return _threshold;
		}


		/**
		 * alpha value threshold
		 * @param value between 0 and 1
		 */
		public function set threshold( value : Number ) : void {
			_threshold = value;
			_multiplier = ( 1 / ( 1 - value ) ) * 2;
			thresholdValues[ 2 ] = _multiplier;
			thresholdValues[ 3 ] = _threshold;
		}
	}
}