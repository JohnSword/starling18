/**
 *	Copyright (c) 2014 Devon O. Wolfgang
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	THE SOFTWARE.
 */

package starling.filters {
	import com.element.oimo.ogsl.OGSL;

	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;

	import starling.textures.Texture;

	/**
	 */
	public class TestFilter extends BaseFilter {

//		private var fc0 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] );
//		private var fc1 : Vector.<Number> = Vector.<Number>( [ 1, 0, 0, 0 ] );
//		private var fc2 : Vector.<Number> = Vector.<Number>( [ 1, 0, 0, 0 ] );
//		private var v0 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] );


		/**
		 */
		public function TestFilter() {

		}


		/** Set AGAL */
		override protected function setAgal() : void {


			VERTEX_SHADER =
			<![CDATA[
m44 op,va0,vc0
mov v0,va1
 			]]>

			FRAGMENT_SHADER =
			<![CDATA[
tex ft0,v0.xy,fs0
mov ft0.x,ft0.y
mov oc,ft0
            ]]>
		}


		/** Activate */
		protected override function activate( pass : int, context : Context3D, texture : Texture ) : void {


//			const fc1 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] )
//			const fc2 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] )
//			const fc3 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] )

//			const fc1 : Vector.<Number> = Vector.<Number>( [ 3.230769, 3.230769, 0, 0 ] )
//			const fc2 : Vector.<Number> = Vector.<Number>( [ 0.07027, 0.316216, 0.227027, 0 ] )
//			const fc0 : Vector.<Number> = Vector.<Number>( [ 1.384615, 1.384615, 0, 0 ] )
//			var direction : Object = { x:1, y:1 };
//			const fc4 : Vector.<Number> = Vector.<Number>( [ direction.x, direction.y, 0, 0 ] )
//			var gl_FragColor : Object = { x:1, y:1, z:1, w:1 };
//			const oc0 : Vector.<Number> = Vector.<Number>( [ gl_FragColor.x, gl_FragColor.y, gl_FragColor.z, gl_FragColor.w ] )
//			var iResolution : Object = { x:1, y:1 };
//			const fc0 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 0 ] ); // lightSpecularColor
//			const fc1 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 0 ] ); // lightAmbientColor
//			const fc2 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 0 ] ); // lightDiffuseColor
//			const fc3 : Vector.<Number> = Vector.<Number>( [ 0.0, 0, 0, 0 ] ); // specIntensity
//			const fc4 : Vector.<Number> = Vector.<Number>( [ 1, 0, 0, 0 ] ); // diffIntensity
//			var gl_FragCoord : Object = { x:1, y:1, z:1, w:1 };
//			const v0 : Vector.<Number> = Vector.<Number>( [ gl_FragCoord.x, gl_FragCoord.y, gl_FragCoord.z, gl_FragCoord.w ] )
//			var fc0 : Vector.<Number> = new <Number>[ 1, 1, 0, 1 ];
//			var gl_FragColor : Object = { x:1, y:1, z:1, w:1 };
//			var oc0 : Vector.<Number> = new <Number>[ gl_FragColor.x, gl_FragColor.y, gl_FragColor.z, gl_FragColor.w ];
//			var vc0 : Vector.<Number> = new <Number>[ 0.5, 0, 0, 0 ];
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 0, fc0, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 1, oc0, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 2, vc0, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 0, fc0, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 1, fc1, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 2, fc2, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 3, fc3, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 4, fc4, 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.VERTEX, 4, v0, 1 );

//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 2, new <Number>[ 0, 1, 3, 2.3 ], 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 3, new <Number>[ 2.5, 2.7, 0.5, 16 ], 1 );
//			context.setProgramConstantsFromVector( Context3DProgramType.VERTEX, 4, new <Number>[ 0, 1, 0, 0 ], 1 );

			super.activate( pass, context, null );
		}

	}
}
