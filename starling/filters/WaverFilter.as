/**
 *	Copyright (c) 2014 Devon O. Wolfgang
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	THE SOFTWARE.
 */

package starling.filters {

	import com.element.oimo.ogsl.OGSL;

	import flash.display3D.Context3D;

	import starling.textures.Texture;

	/**
	 */
	public class WaverFilter extends BaseFilter {

//		private var fc0 : Vector.<Number> = Vector.<Number>( [ 1, 1, 1, 1 ] );

		private var ogsl : OGSL;
		private var count : Number = 0;
		private var speed : Number;
		private var bendFactor : Number;


		/**
		 */
		public function WaverFilter( speed : Number = 2, bendFactor : Number = 0.2 ) : void {
			super();
			this.speed = speed;
			this.bendFactor = bendFactor;
		}


		/** Set AGAL */
		override protected function setAgal() : void {

			ogsl = new OGSL();
			ogsl.setLogFunction( function( s : String ) : void { // set log function
				trace( s );
			} );

			var vertexShader : String = <![CDATA[
varying pos:vec2;
varying uv:vec2;
program vertex {
	attribute position:vec3;
	attribute texCoord:vec2;
	uniform matrix:mat4x4;

	function main():void {
		var pos:vec4 = mul(vec4(position, 1), matrix);
		output = pos;
		this.pos = pos.xy;
		this.uv = texCoord;
	}
}
]]>;


			var displayFragmentShader : String = <![CDATA[
program fragment {
	
	uniform tex1 : texture;
	uniform speed:float;
	uniform bendFactor:float;
	uniform time:float;

    function main():void {
		var height:float = 1.0 - uv.y;
		var offset:float = pow(height, 2.5);
		 
		// multiply by sin since it gives us nice bending
		offset *= (sin(time * speed) * bendFactor);
		 
		var normalColor:vec4 = tex2D(tex1, fract(vec2(uv.x + offset, uv.y)), clamp, linear, nomip);
		var color:vec4 = normalColor;

        output = color;
    }

}
]]>;

			ogsl.compile( vertexShader + " " + displayFragmentShader );


			VERTEX_SHADER = ogsl.getVertexAGAL();
			FRAGMENT_SHADER = ogsl.getFragmentAGAL();

		}


		/** Activate */
		protected override function activate( pass : int, context : Context3D, texture : Texture ) : void {

			ogsl.setContext3D( context );
			ogsl.setDefaultConstants();

			count++;

			ogsl.setFragmentConstantsFromNumber( "time", Math.sin( count / 30 ) );
			ogsl.setFragmentConstantsFromNumber( "speed", this.speed );
			ogsl.setFragmentConstantsFromNumber( "bendFactor", this.bendFactor );

			super.activate( pass, context, texture );
		}

	}
}
