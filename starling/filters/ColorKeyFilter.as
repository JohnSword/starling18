// =================================================================================================
//
//	Starling Framework
//	Copyright 2012 Gamua OG. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package starling.filters
{
    import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
    import flash.display3D.Program3D;
	import starling.filters.FragmentFilter;
	import starling.filters.FragmentFilterMode;
	import starling.utils.Color;
    
    import starling.textures.Texture;

    public class ColorKeyFilter extends FragmentFilter
    {
        private var mShaderProgram:Program3D;
		private var colorValues:Vector.<Number>;
		
		private var _color:uint;
		private var _threshold:Number;
		
        public function ColorKeyFilter ( color:uint = 0x000000, threshold:Number = .5 )
        {			
			//Fragment Constatns			
			colorValues = new <Number>[0,0,0,0]; //colors with alpha
			
			this.threshold = threshold;
			this.color = color;	
        
			super();		
			mode = FragmentFilterMode.REPLACE;
        }
        
        public override function dispose():void
        {
            if (mShaderProgram) mShaderProgram.dispose();		
			colorValues = null;
            super.dispose();
        }
        
        protected override function createPrograms():void
        {
            var fragmentProgramCode:String =
                "tex ft0, v0, fs0 <2d, clamp, linear, mipnone> \n" + // just forward texture color				
				"sub ft1, ft0, fc0 \n" + 							//check pixel value
				"add ft2.x, ft1.x, ft.y \n" + 						//add values
				"add ft2.x, ft2.x, ft.z \n" + 						//add values
				"sge ft2.w, ft.x, fc0.w \n" +						//check value <= threshold
				"mul ft0, ft0, ft2.w \n"+
				"mov oc, ft0 \n";									//output
				
            mShaderProgram = assembleAgal(fragmentProgramCode);
        }
        
        protected override function activate(pass:int, context:Context3D, texture:Texture):void
        {
            // already set by super class:
            // 
            // vertex constants 0-3: mvpMatrix (3D)
            // vertex attribute 0:   vertex position (FLOAT_2)
            // vertex attribute 1:   texture coordinates (FLOAT_2)
            // texture 0:            input texture
      
			context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, colorValues, 1);
            context.setProgram(mShaderProgram);			
        }
		
		public function get threshold():Number 
		{
			return _threshold;
		}
		
		public function set threshold(value:Number):void 
		{
			_threshold = value;
			colorValues[3] = value;
		}
		
		public function get color():uint 
		{
			return _color;
		}
		
		public function set color(value:uint):void 
		{
			_color = value;
			colorValues[0] = Color.getRed (value) / 255;
			colorValues[1] = Color.getGreen (value) / 255;
			colorValues[2] = Color.getBlue (value) / 255;				
		}
    }
}