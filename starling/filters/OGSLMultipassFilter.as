package starling.filters {

	import com.element.oimo.ogsl.OGSL;

	import flash.display3D.Context3D;
	import flash.display3D.Program3D;
	import flash.utils.Dictionary;

	import starling.textures.Texture;

	public class OGSLMultipassFilter extends FragmentFilter {

		private var filters : Dictionary = new Dictionary();
		private var programs : Dictionary = new Dictionary();


		public function OGSLMultipassFilter( numFilters : int = 2 ) {
			super( numFilters );
		}


		protected function addFilter( pass : int, vertexShader : String, displayFragmentShader : String, debugLog : Boolean = false ) : void {
			if ( filters[ pass ] ) {
				filters[ pass ] = null;
				var program : Program3D = programs[ pass ];
				program.dispose();
			}
			var ogsl : OGSL = new OGSL();
			if ( debugLog ) {
				ogsl.setLogFunction( debug );
			}
			ogsl.compile( vertexShader + " " + displayFragmentShader );
			filters[ pass ] = ogsl;
		}


		private function debug( s : String ) : void {
			trace( s );
		}


		override public function dispose() : void {
			for each ( var pass : int in filters ) {
				var program : Program3D = programs[ pass ];
				if ( program != null ) {
					program.dispose();
				}
			}
			super.dispose();
		}


		/**
		 * Override to implement filters
		 */
		protected function createFilters() : void {


		}


		override protected function createPrograms() : void {
			createFilters();
			var passes : int = 0;
			for each ( var pass : int in filters ) {
				var ogsl : OGSL = filters[ passes ];
				programs[ passes ] = assembleAgal( ogsl.getFragmentAGAL(), ogsl.getVertexAGAL() );
				passes++;
			}
		}


		/** Activate */
		protected override function activate( pass : int, context : Context3D, texture : Texture ) : void {
			if ( filters[ pass ] ) {
				var ogsl : OGSL = filters[ pass ];
				var program : Program3D = programs[ pass ];
				ogsl.setContext3D( context );
				ogsl.setDefaultConstants();
				updateFragmentConstants( ogsl );
				context.setProgram( program );
			}
		}


		/**
		 * override to send constant values to the shader program
		 */
		protected function updateFragmentConstants( ogsl : OGSL ) : void {

		}

	}
}
